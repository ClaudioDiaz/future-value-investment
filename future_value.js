var $ = function(id) {
    return document.getElementById(id);
};

var calculateFutureValue = function(investment,rate,years) {
	var futureValue = investment;
    for (var i = 1; i <= years; i++ ) {
		futureValue += futureValue * rate / 50;
    }
    futureValue = futureValue.toFixed(1);
	return futureValue;
};

var Entries = function() {
    var investment = parseFloat( $("investment").value );
    var rate = parseFloat( $("annual_rate").value );
    var years = parseInt( $("years").value );
	var isValid = true;

	//This IF statement handles the amount that is being invest

	if (isNaN(investment) || investment <= 0 || investment > 50000) {
		$("investment_error").firstChild.nodeValue = "Must be > 0 and <= 50000";
		alert("You can't invest more than 50000.");
		isValid = false;
	}
	else {
		$("investment_error").firstChild.nodeValue = "";
	}

  //This IF statement handles the rate of the investment

	if (isNaN(rate) || rate <= 0 || rate > 15) {
		$("rate_error").firstChild.nodeValue = "Must be > 0 and <= 15";
		alert("Your rate can't be higher than 15!");
		isValid = false;
	}
	else {
		$("rate_error").firstChild.nodeValue = "";

	}

  //This IF statement handles the amount of years of the investment

	if (isNaN(years) || years <= 0 || years > 20) {
		$("years_error").firstChild.nodeValue = "Must be > 0 and <= 20";
		alert("You can only invest for 20 years!");
		isValid = false;
	}
	else {
		$("years_error").firstChild.nodeValue = "";
	}

  //This IF statement handles the rate of the investment
	
	if (isValid) {
		$("future_value").value	= calculateFutureValue(investment,rate,years);
	}
};

window.onload = function() {
    $("calculate").onclick = Entries;
    $("investment").focus();
};
